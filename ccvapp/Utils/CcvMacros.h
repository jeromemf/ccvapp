//
//  CcvMacros.h
//  ccvapp
//
//  Created by Jerome on 09/08/16.
//
//

#ifndef CcvMacros_h
#define CcvMacros_h

#define DEVICE_PAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define DEVICE_PHONE (!DEVICE_PAD)

#define DEVICE_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2)

#define DEVICE_BOUNDS ([UIScreen mainScreen].bounds)
#define DEVICE_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define DEVICE_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define DEVICE_STATUS_BAR_HEIGHT ([UIApplication sharedApplication].statusBarFrame.size.height)

#define DEVICE_KEYBOARD_HEIGHT (DEVICE_PHONE ? 216.0 : 352.0)

#endif /* CcvMacros_h */
