//
//  ButtonsViewController.h
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import <UIKit/UIKit.h>

@interface ButtonsViewController : UIViewController

- (void)presentDoneButton:(NSNotificationCenter *)notification;
- (void)addBackButton:(NSNotificationCenter *)notification;

@end

