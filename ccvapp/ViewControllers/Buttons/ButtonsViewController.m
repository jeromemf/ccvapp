//
//  ButtonsViewController.m
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import "ButtonsViewController.h"

@interface ButtonsViewController ()

@property (strong, nonatomic) UIView *containerView;
@property (retain, nonatomic) UIButton *done;
@property (retain, nonatomic) UIButton *back;
@property (retain, nonatomic) UIButton *next;

@end

@implementation ButtonsViewController

+ (instancetype)sharedInstance {
    static ButtonsViewController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ButtonsViewController alloc] init];
    });
    return sharedInstance;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self addNotifications];
    [self addContentView];
}

#pragma mark - Initialization

-(void)addNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addBackButton:)
                                                 name:@"kaddBackButtonNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(presentDoneButton:)
                                                 name:@"kpresentDoneButtonNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeDoneButton:)
                                                 name:@"kRemoveDoneButtonNotification"
                                               object:nil];
}

-(void)addContentView{
    self.done = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.done addTarget:self
                  action:@selector(checkCard)
        forControlEvents:UIControlEventTouchUpInside];
    [self.done setTitle:@"Done" forState:UIControlStateNormal];
    [self.done.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];

    
    self.back = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.back addTarget:self
                  action:@selector(moveBack)
        forControlEvents:UIControlEventTouchUpInside];
    [self.back setTitle:@"Back" forState:UIControlStateNormal];
    [self.back.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    self.back.frame = CGRectMake(0, -10, DEVICE_WIDTH/2, 70);
    
    self.next = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.next addTarget:self
                  action:@selector(moveNext)
        forControlEvents:UIControlEventTouchUpInside];
    [self.next setTitle:@"Next" forState:UIControlStateNormal];
    [self.next.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    self.next.frame = CGRectMake(0, -10, DEVICE_WIDTH, 70);
    [self.view addSubview:self.next];
}

#pragma mark - Buttons slide

- (void)presentDoneButton:(NSNotificationCenter *)notification{
    /**************************************
     * Remove Next and Back Buttons
     ***************************************/
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.next setFrame:CGRectMake(-400, -10, DEVICE_WIDTH, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Removed next!");
                     }
     ];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.back setFrame:CGRectMake(-400, -10, DEVICE_WIDTH/2, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Removed back!");
                     }
     ];
    
    
    /**************************************
     * Place Done button
     ***************************************/
    [self.view addSubview:self.done];
    [self.done setFrame:CGRectMake(400, -10, DEVICE_WIDTH, 70)];
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.done setFrame:CGRectMake(0, -10, DEVICE_WIDTH, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Added done!");
                     }
     ];
}

- (void)addBackButton:(NSNotificationCenter *)notification{
    NSLog(@"JEROME");
    /**************************************
     * Push Next and add Back Buttons
     ***************************************/
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.next setFrame:CGRectMake(DEVICE_WIDTH/4, -10, DEVICE_WIDTH, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Moved next!");
                     }
     ];
    
    [self.view addSubview:self.back];
    [self.back setFrame:CGRectMake(-400, -10, DEVICE_WIDTH/2, 70)];
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.back setFrame:CGRectMake(0, -10, DEVICE_WIDTH/2, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Added back!");
                     }
     ];
    
//    [self performSelector:@selector(presentDoneButton) withObject:nil afterDelay:2];
}

- (void)removeDoneButton:(NSNotificationCenter *)notification{
    /**************************************
     * Add Next and Back Buttons
     ***************************************/
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.next setFrame:CGRectMake(DEVICE_WIDTH/4, -10, DEVICE_WIDTH, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Removed next!");
                     }
     ];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.back setFrame:CGRectMake(0, -10, DEVICE_WIDTH/2, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Removed back!");
                     }
     ];
    
    
    /**************************************
     * Remove Done button
     ***************************************/
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         // set the new frame
                         [self.done setFrame:CGRectMake(400, -10, DEVICE_WIDTH, 70)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Added done!");
                     }
     ];
}
@end
