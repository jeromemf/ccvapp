//
//  CardsPresenterViewController.m
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import "CardsPresenterViewController.h"

#define ccPosX        0.0
#define ccPosY        0.0
#define ccWidth     self.containerView.frame.size.width
#define ccHeight    self.containerView.frame.size.height

@interface CardsPresenterViewController ()

@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UIImageView *frontView;
@property (strong, nonatomic) UIImageView *backView;
@property (strong, nonatomic) UIImageView *gCardView;

@end

@implementation CardsPresenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self addNotifications];
    [self addContentView];
}

#pragma mark - Initialization

-(void)addNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadCard:)
                                                 name:@"kLoadCardNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(flipCard:)
                                                 name:@"kFlipCardNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(flipCardBack:)
                                                 name:@"kFlipCardBackNotification"
                                               object:nil];
}

-(void)addContentView{
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake((DEVICE_BOUNDS.size.width - 280.0)/2, 50.0, 280, 174)];
    [self.containerView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.containerView];
    
    
    self.gCardView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GenericCard"]];
    [self.gCardView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    
    [self.containerView addSubview:self.gCardView];
}

#pragma mark - Card Transition

- (void)loadCard:(NSNotification *)notification{
    
    NSDictionary *userInfo = [notification userInfo];
    NSNumber *cardNum = [userInfo objectForKey:@"Card"];
    int card = [cardNum intValue];
    
    if (card == 1) {
        
        self.frontView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VisaFront"]];
        self.backView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"VisaBack"]];
        
    } else {
        
        self.frontView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MCFront"]];
        self.backView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MCBack"]];
        
    }
    
    [self.frontView setFrame:CGRectMake(ccPosX, ccPosY, 0, ccHeight)];
    [self.backView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.frontView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
                     }
                     completion:^(BOOL finished) {
                         
                     }];
    
    [self.containerView addSubview:self.frontView];
}

#pragma mark - Card Flip

-(void)flipCard:(NSNotification *)notification{
    [self.frontView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    [self.backView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    
    
    [CATransaction flush];
    
    [UIView transitionFromView:self.frontView
                        toView:self.backView
                      duration:1
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    completion:NULL];
}

-(void)flipCardBack:(NSNotification *)notification{
    [self.frontView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    [self.backView setFrame:CGRectMake(ccPosX, ccPosY, ccWidth, ccHeight)];
    
    
    [CATransaction flush];
    
    [UIView transitionFromView:self.backView
                        toView:self.frontView
                      duration:1
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    completion:NULL];
}

@end
