//
//  CardsPresenterViewController.h
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@interface CardsPresenterViewController : UIViewController

-(void)loadCard:(NSNotification *)notification;
-(void)flipCard:(NSNotification *)notification;
-(void)flipCardBack:(NSNotification *)notification;

@end
