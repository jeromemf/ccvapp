//
//  HomeViewController.m
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self initInterface];
    
//    [self startHome:NO];
    [self displayContentController];
    

}



/**********************************************
 *
 * INTERFACE METHODS
 *
 *********************************************/

- (void) initInterface {
    
    self.rootNC = [[UINavigationController alloc] init];
    self.rootNC.navigationBarHidden = YES;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.rootNC.view];
}

#pragma mark - display child views

- (void)displayContentController{
    
    CardsPresenterViewController *cardsView = [[CardsPresenterViewController alloc] init];
    [self addChildViewController:cardsView];
    [cardsView.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, 240)];
    [cardsView.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:cardsView.view];
    [cardsView didMoveToParentViewController:self];
    
    FormViewController *formView = [[FormViewController alloc] init];
    [self addChildViewController:formView];
    [formView.view setFrame:CGRectMake(0, 240, self.view.frame.size.width, 60)];
    [formView.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:formView.view];
    [formView didMoveToParentViewController:self];
    
    ButtonsViewController *buttonsView = [[ButtonsViewController alloc] init];
    [self addChildViewController:buttonsView];
    [buttonsView.view setFrame:CGRectMake(0, 300, self.view.frame.size.width, 52)];
    [buttonsView.view setBackgroundColor:[UIColor colorWithRed:0.42 green:0.66 blue:0.31 alpha:1.0]];
    [self.view addSubview:buttonsView.view];
    [buttonsView didMoveToParentViewController:self];
}

//- (void) startHome:(BOOL)logout {
//    
//    [self disposeMain];
//    
//    self.homeVC = [[NWHomeViewController alloc] init];
//    
//    [self.rootNC setViewControllers:@[self.homeVC] animated:YES];
//    
//    if (logout) {
//        [self.homeVC logout];
//    }
//    else {
//        [self.homeVC getBootstrap];
//    }
//}
//
//
//- (void) startMain {
//    
//    [self disposeMain];
//    
//    self.mainVC = [[NWMainViewController alloc] init];
//    
//    [self.rootNC setViewControllers:@[self.mainVC] animated:YES];
//}
//
//
//- (void) disposeMain {
//    
//    [self.mainVC exitMain];
//    
//    self.mainVC = nil;
//}

@end
