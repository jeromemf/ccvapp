//
//  HomeViewController.h
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import "CardsPresenterViewController.h"
#import "FormViewController.h"
#import "ButtonsViewController.h"

@interface HomeViewController : UIViewController

@property (nonatomic, retain) UINavigationController *rootNC;

//@property (nonatomic, retain) NWHomeViewController *homeVC;
//@property (nonatomic, retain) NWMainViewController *mainVC;

@end
