//
//  FormViewController.m
//  ccvapp
//
//  Created by Jerome on 09/08/16.
//
//

#import "FormViewController.h"

@interface FormViewController () <UITextFieldDelegate>

@property (strong, nonatomic) UIView *containerView;

@end

@implementation FormViewController
{
    UITextField *number;
    UITextField *ccvNum;
    UITextField *invField;
    BOOL isFlipped;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //    [UIView beginAnimations:nil context:nil];
    //    [UIView setAnimationDuration:0.0];
    //    [UIView setAnimationDelay:0.0];
    //    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    [self initViewElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [number becomeFirstResponder];
    [number addTarget:self
               action:@selector(checkCardType:)
     forControlEvents:UIControlEventEditingChanged];
}

#pragma mark - Init elements

-(void)initViewElements{
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 60)];
    [self.view addSubview:self.containerView];
    
    number = [[UITextField alloc] initWithFrame:CGRectMake(10, 15, 200, 40)];
    [number setBorderStyle:UITextBorderStyleNone];
    [number setKeyboardType:UIKeyboardTypeNumberPad];
    [number setReturnKeyType:UIReturnKeyDone];
    [number setTag:1];
    [number addTarget:self action:@selector(numberEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, -15, 200, 40)];
    numberLabel.text = @"CARD NUMBER";
    [numberLabel setTextColor:[UIColor grayColor]];
    
    ccvNum = [[UITextField alloc] initWithFrame:CGRectMake(220, 15, 90, 40)];
    [ccvNum setBorderStyle:UITextBorderStyleNone];
    [ccvNum setKeyboardType:UIKeyboardTypeNumberPad];
    [ccvNum setReturnKeyType:UIReturnKeyDone];
    [ccvNum setTag:2];
    [ccvNum addTarget:self action:@selector(ccvNumEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    
    invField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH+100, DEVICE_HEIGHT+100)];
    [invField setBorderStyle:UITextBorderStyleNone];
    [invField setKeyboardType:UIKeyboardTypeNumberPad];
    
    UILabel *ccvLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, -15, 200, 40)];
    ccvLabel.text = @"CCV/CVC";
    [ccvLabel setTextColor:[UIColor grayColor]];
    
    [self.containerView addSubview:number];
    [self.containerView addSubview:ccvNum];
    [self.containerView addSubview:invField];
    [self.containerView addSubview:numberLabel];
    [self.containerView addSubview:ccvLabel];
}

#pragma mark - Animations

-(void)checkCardType:(UITextField *)theTextField{
    
    if (theTextField.text.length == 1) {
        NSString *firstLetter = theTextField.text;
        
        if ([firstLetter isEqual: @"0"] || [firstLetter isEqual: @"1"] || [firstLetter isEqual: @"2"] || [firstLetter isEqual: @"3"] || [firstLetter  isEqual: @"4"]) {
            NSDictionary *userInfo = @{@"Card" : @1};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kLoadCardNotification" object:nil userInfo:userInfo];
        }else{
            NSDictionary *userInfo = @{@"Card" : @2};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kLoadCardNotification" object:nil userInfo:userInfo];
        }
    }
}

- (void)numberEditingChanged:(id)sender{
    
    if ([number isFirstResponder] && isFlipped) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kFlipCardBackNotification" object:nil userInfo:nil];
    }
    
    if(number.text.length == 1){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kaddBackButtonNotification" object:nil];
    }
    if(number.text.length == 19){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kFlipCardNotification" object:nil userInfo:nil];
        isFlipped = YES;
        [ccvNum becomeFirstResponder];
    }
    
    if (number.text.length < 19 && ccvNum.text.length == 3) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kRemoveDoneButtonNotification" object:nil];
    }
}

- (void)ccvNumEditingChanged:(id)sender{
    if (ccvNum.text.length == 3) {
        [number becomeFirstResponder];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kpresentDoneButtonNotification" object:nil];
    }
    
    if(ccvNum.text.length < 3 && number.text.length != 0 && isFlipped){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kRemoveDoneButtonNotification" object:nil];
    }
}

@end
