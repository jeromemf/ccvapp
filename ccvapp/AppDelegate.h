//
//  AppDelegate.h
//  ccvapp
//
//  Created by Jerome on 08/08/16.
//
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *appNC;

@property (strong, nonatomic) HomeViewController* rootVC;

@end

